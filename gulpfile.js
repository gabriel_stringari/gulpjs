/**
 * Caso a compilação do less não esteje funcionando, rodar o seguinte comando no seu htdocs:
 * npm install --save-dev vinyl gulp-dependencies-changed map-stream find-in-file
 */
var pluginsDir = __dirname.replace(/\\/g, '/') + '../gulpjs/node_modules/'

var projectFolder = 'default-modules-frontend'
var pathToDefaultModules = './../' + projectFolder + '/modules/'
var defaultModuleName = 'default'

function currentDirectory() {
    if(__dirname.search('/') < 0) {
        return __dirname.split(/(\\)/).pop();
    } else {
        return __dirname.split('/(/)/').pop();
    }
}

function camelCaser(input) {
    return input.replace(/[- _](.)/g, function(match, group1) {
        return group1.toUpperCase()
    })
}

function loadPlugin(name) {
    if(require.resolve(name)) {
        // console.log(require.resolve(name));
        return require(name)
    }

    // console.log(require.resolve(pluginsDir + name));
    return require(pluginsDir + name)
}

var gulp = require('gulp'),
    //fs = loadPlugin('fs'),
    //argv = loadPlugin('yargs').argv,
    config = {
        paths: {
            defaultModule: pathToDefaultModules + defaultModuleName,
            defaultModuleLess: pathToDefaultModules + defaultModuleName + '/assets/less',
            defaultModuleSass: pathToDefaultModules + defaultModuleName + '/assets/scss',
            bowerDir: 'bower_components',
            less: {
                srcAll:  'application/modules',
                main:    'application/modules/comum/assets/less/main.less',
                mainDir: 'application/modules/comum/assets/less',
            },
            sass: {
                srcAll:  'application/modules',
                main:    'application/modules/comum/assets/less/main.scss',
                mainDir: 'application/modules/comum/assets/scss',
            },
            js: {
                srcAll:  ['application/modules/**/assets/js/*.js','!application/modules/**/assets/js/*.min.js']
            },
            maps: {
                srcUrl: '/application/modules/comum/assets/sourcemaps'
            }
        }
    },
    plugins = {
        //cssmin:      loadPlugin('gulp-minify-css'),
        cleanCSS:    loadPlugin('gulp-clean-css'),
        debug:       loadPlugin('gulp-debug'),
        gulpReplace: loadPlugin('gulp-replace'),
        less:        loadPlugin('gulp-less'),
        livereload:  loadPlugin('gulp-livereload'),
        moment:      loadPlugin('moment'),
        notify:      loadPlugin('gulp-notify'),
        rename:      loadPlugin('gulp-rename'),
        sourcemaps:  loadPlugin('gulp-sourcemaps'),
        util:        loadPlugin('gulp-util'),
        vinyl:       loadPlugin('vinyl'),
        changed:     loadPlugin('gulp-dependencies-changed'),
        map:         loadPlugin('map-stream'),
        findInFile:  loadPlugin('find-in-file')
    };


function getFilesThatContain(file){
    var regex = new RegExp('@import [\'"]?([^\'"]+)?('+file.replace('.','\\.')+')[\'"]?;', "g"),
        files = [],
        promises = [];
    return new Promise(
        (resolve) => {
        gulp.src(['application/modules/**/*.less', '!**/_*/**/*.less'])
        .pipe(plugins.map(function (file, cb) {
            plugins.findInFile({
                files: file.path,
                find: regex
            }, function(err, matchedFiles) {
                if (matchedFiles.length){
                    matchedFiles.forEach((data) => {
                        var f = data.file.split('\\').pop();
                    if ((m = /^_/g.exec(f)) !== null) {
                        promises.push(getFilesThatContain(f).then((fs)=>{
                            files = files.concat(fs);
                    }));
                    }else{
                        files.push(data.file);
                    }
                });
                }
            });
            cb(null, file);
        })).on('end', function(){
            Promise.all(promises).then(function(values) {
                resolve(files)
            });
        });
}
);
}

function css(file){
    var lessFile = new plugins.vinyl({ path: file.path }),
        basePath = (lessFile.path).replace(lessFile.relative,''),
        lessPromise = new Promise((resolve) => {
        // Confere se arquivo começa com _
        if ((m = /^_/g.exec(lessFile.basename)) !== null) {
            getFilesThatContain(lessFile.basename).then((files)=>{
                resolve(files);
            });
        }else{
            // Pega o arquivo a ser alterado
            resolve([lessFile.path]);
        }
    }).then((files)=>{
            // Compila e minifica os arquivos selecionados até aqui:
            return gulp.src(files, { base: 'application/modules' })
                .pipe(
                    plugins.less({
                        paths: [
                            '.',
                            './node_modules',
                            './bower_components',
                            'application/modules/comum/assets/less'
                        ]
                    }).on("error", plugins.notify.onError(function (error) {
                        return "Error: " + error.message + '\n' + error.line + ':' + error.index
                    }))
                )
                .pipe(plugins.cleanCSS({compatibility:'ie9'}))
                .pipe(plugins.rename(function(path) {
                    path.dirname += '/../css';
                }))
                .pipe(gulp.dest('application/modules'))
                .pipe(plugins.debug({title:'Compiled LESS: '}))
                .pipe(plugins.notify({
                    message:'Compiled LESS',
                    onLast:true
                }));
    });
};

//Watches for changes and executes the tasks automatically
gulp.task('watch', function() {
    //plugins.livereload.listen()
    gulp.watch(config.paths.less.srcAll + '/**/*.less', css);
    gulp.watch(config.paths.js.srcAll, ['compress'])
    // gulp.watch(config.paths.sass.srcAll + '/**/*.scss', ['sass'])
});

gulp.task('compress', function() {
    var minify = require(pluginsDir + 'gulp-minify');
    gulp.src(config.paths.js.srcAll)
        .pipe(minify({
            ext:{
                min:'.min.js'
            },
            noSource: true,
            ignoreFiles: ['.min.js', '-min.js']
        }).on('error', plugins.notify.onError({
            message: "Error: <%= error.message %>",
            title: "Syntax error"
        })))
        .pipe(gulp.dest('application/modules'))
        .pipe(plugins.notify({
            message:'Compressed JS (' + plugins.moment().format('MMM Do h:mm:ss A') + ')',
            onLast:true
        }));
});

gulp.task('default', ['watch','compress']);
gulp.task('dev', ['watch']);
gulp.task('dist', ['compress']);